import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://161.35.4.77:8082/api/test/";

class UserService {
  getPublicContent() {
    return axios.get(API_URL + "all");
  }

  getUserBoard() {
    return axios.get(API_URL + "sponsor", { headers: authHeader() });
  }

  getModeratorBoard() {
    return axios.get(API_URL + "organizer", { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(API_URL + "admin", { headers: authHeader() });
  }
}

export default new UserService();
